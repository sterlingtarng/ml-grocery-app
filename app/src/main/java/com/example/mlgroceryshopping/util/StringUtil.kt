package com.example.mlgroceryshopping.util

import java.lang.Exception

fun String.isAllUppercase(): Boolean {
    for (i in 0 until this.length) {
        if (Character.isLowerCase(this[i])) {
            return false
        }
    }
    return true
}

fun String.hasNumbers(exception: String): Boolean {
    if (exception in this) {
        return false
    }
    for (i in 0 until this.length) {
        if (Character.isDigit(this[i])) {
            return true
        }
    }
    return false
}

fun String.hasAlpha(): Boolean {
    for (element in this) {
        if (element in 'A'..'Z') {
            return true
        }
    }
    return false
}

fun String.isFloat(): Boolean {
    return try {
        this.toFloat()
        true
    } catch (exception: Exception) {
        false
    }
}

fun String.dateFormat(): String {
    try {
        val value = Integer.parseInt(this)
        return if (value < 10) {
            "0$value"
        } else {
            this
        }

    } catch (exception: Exception) {
        return this
    }
}