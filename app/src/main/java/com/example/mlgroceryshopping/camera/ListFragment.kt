package com.example.mlgroceryshopping.camera

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ListView
import android.widget.TextView
import androidx.core.view.iterator
import androidx.lifecycle.ViewModelProvider
import com.example.mlgroceryshopping.R
import com.example.mlgroceryshopping.food_modeldb.Food
import com.example.mlgroceryshopping.food_modeldb.FoodDatabase
import com.example.mlgroceryshopping.food_modeldb.FoodViewModel
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.io.FileWriter
import java.io.InputStream
//import com.google.
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*
import java.util.concurrent.TimeUnit

class ListFragment : CameraFragment() {
    lateinit var ListHere: ListView
    lateinit var saveButton: Button
    lateinit var startOver: Button
    private lateinit var foodViewModel: FoodViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.activity_list, container, false)
        val arraylist = requireArguments().getStringArrayList("Values")

        ListHere = view.findViewById(R.id.parsedList2)
        ListHere.adapter = ListItemAdapter(this.requireContext(), R.layout.activity_list_item, arraylist!!)
        foodViewModel = ViewModelProvider(this).get(FoodViewModel::class.java)

        saveButton = view.findViewById(R.id.saveList)

        startOver = view.findViewById(R.id.startOver)
        startOver.setOnClickListener{
            val nextFrag = CameraFragment.newInstance()
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.fragment_container, nextFrag, "findThisFragment")
                ?.addToBackStack(null)
                ?.commit()
        }
        saveButton.setOnClickListener {
//            Save to database
            for (food in ListHere){
                var foodName : TextView = food.findViewById(com.example.mlgroceryshopping.R.id.foodName)
                var purchaseDate : TextView = food.findViewById(com.example.mlgroceryshopping.R.id.purchaseLabel)
                var expirationDate: TextView = food.findViewById(com.example.mlgroceryshopping.R.id.expLabel)
                var foods = Food(foodName.text.toString(), LocalDate.parse(purchaseDate.text),LocalDate.parse(expirationDate.text.toString()))
                //add food to JSON
//                addToJSON(foods)
                foodViewModel.insert(foods)
            }
            val nextFrag = CameraFragment.newInstance()
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.fragment_container, nextFrag, "findThisFragment")
                ?.addToBackStack(null)
                ?.commit()
        }
        return view
    }
    companion object {
        fun newInstance(): ListFragment =
            ListFragment()
    }
}