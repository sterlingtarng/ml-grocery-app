package com.example.mlgroceryshopping.camera

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.room.util.StringUtil
import com.example.mlgroceryshopping.R
import com.example.mlgroceryshopping.util.hasAlpha
import com.example.mlgroceryshopping.util.hasNumbers
import com.example.mlgroceryshopping.util.isAllUppercase
import com.example.mlgroceryshopping.util.isFloat
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.text.FirebaseVisionText

open class CameraFragment : Fragment () {
    lateinit var imageView: ImageView
    lateinit var textView: TextView
    lateinit var mListView: ListView
    lateinit var selectImage: Button
    lateinit var recognizeImage: Button
    var values: MutableList<String> = ArrayList()
    lateinit var arrayAdapter: ArrayAdapter<*>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_camera, container, false)

        imageView = view.findViewById(com.example.mlgroceryshopping.R.id.imageView)

        textView = view.findViewById(com.example.mlgroceryshopping.R.id.editText)
        selectImage = view.findViewById(R.id.selectImage)
        recognizeImage = view.findViewById(R.id.recognizeText)
        arrayAdapter = ArrayAdapter(this.requireContext(),
            android.R.layout.simple_list_item_1, values)

        selectImage.setOnClickListener {
            selectImage(view)
        }

        recognizeImage.setOnClickListener {
            startRecognizing(view)
        }
        return view
    }

    fun selectImage(v: View) {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            imageView.setImageURI(data!!.data)

        }
    }

    fun startRecognizing(v: View) {
        if (imageView.drawable != null) {
            textView.setText("")
            v.isEnabled = false
            val bitmap = (imageView.drawable as BitmapDrawable).bitmap
            val image = FirebaseVisionImage.fromBitmap(bitmap)
            val detector = FirebaseVision.getInstance().onDeviceTextRecognizer

            detector.processImage(image)
                .addOnSuccessListener { firebaseVisionText ->
                    v.isEnabled = true
                    processResultText(firebaseVisionText)
                }
                .addOnFailureListener {
                    v.isEnabled = true
                    textView.setText("Failed")
                }
        } else {
            Toast.makeText(this.requireContext(), "Select an Image First", Toast.LENGTH_LONG).show()
        }

    }


    private fun processResultText(resultText: FirebaseVisionText) {
        if (resultText.textBlocks.size == 0) {
            textView.setText("No Text Found")
            return
        }

        lateinit var parsedValues: List<String>
        val header = resultText.textBlocks[0].lines[0].text
        when {
            "SAFEWAY" in header -> {
                parsedValues = parseSafewayReceipt(resultText)
            }
            "TARGET" in header -> {
                parsedValues = parseTargetReceipt(resultText)
            }
            else -> {
                parsedValues = parseReceipt(resultText)
            }
        }

//        for (block in resultText.textBlocks) {
//            val blockText = block.text
//            val blockConfidence = block.confidence
//            for (line in block.lines) {
//                val lineText = line.text
//                val lineConfidence = line.confidence
//                values.add(lineText)
//            }
//        }
//        arrayAdapter = ArrayAdapter(
//            this.requireContext(),
//            android.R.layout.simple_list_item_1, values
//        )
//        mListView.adapter = arrayAdapter
        textView.setText("")
        val nextFrag = ListFragment.newInstance()
        val args = Bundle()
        args.putStringArrayList("Values", ArrayList<String>(parsedValues))
        nextFrag.setArguments(args);
        activity?.supportFragmentManager?.beginTransaction()
            ?.replace(R.id.fragment_container, nextFrag, "findThisFragment")
            ?.addToBackStack(null)
            ?.commit()
    }

    private fun parseSafewayReceipt(resultText: FirebaseVisionText): List<String> {
        val parsedValues: MutableList<String> = ArrayList()

        for (block in resultText.textBlocks) {
            for (line in block.lines) {
                val lineText = line.text
                val blackList = listOf("SAFEWAY", "GROCERY", "REFRIG/FROZEN",
                    "PRODUCE", "MEAT", "MISCELLANEOUS", "MR", "TAX")
                var blackListed = false
                for (item in blackList) {
                    if (item == lineText) {
                        blackListed = true
                    }
                }
                if (!blackListed && baseCheck(lineText)
                    && !lineText.hasNumbers("QTY")) {
                    parsedValues.add(lineText)
                }
            }
        }

        return parsedValues.toList()
    }

    private fun parseTargetReceipt(resultText: FirebaseVisionText): List<String> {
        val parsedValues: MutableList<String> = ArrayList()

        for (block in resultText.textBlocks) {
            for (line in block.lines) {
                val lineText = line.text
                val blackList = listOf("TARGET", "OTARGET","NF", "STATIONERY & OFFICE SUPPLIES", "T", "GROCERY", "NON RETAIL")
                var blackListed = false
                for (item in blackList) {
                    if (item == lineText) {
                        blackListed = true
                    }
                }
                if (lineText == "SUBTOTAL") {
                    return parsedValues.toList()
                }
                if (!blackListed && baseCheck(lineText) && '$' !in lineText) {
                    parsedValues.add(lineText)
                }
            }
        }

        return parsedValues.toList()
    }

    private fun parseReceipt(resultText: FirebaseVisionText): List<String> {
        val parsedValues: MutableList<String> = ArrayList()

        for (block in resultText.textBlocks) {
            for (line in block.lines) {
                val lineText = line.text
                if (baseCheck(lineText)) {
                    parsedValues.add(lineText)
                }
            }
        }

        return parsedValues.toList()
    }

    private fun baseCheck(lineText: String): Boolean {
        return lineText.isAllUppercase() && lineText.hasAlpha() && !lineText.isFloat()
    }

    companion object {
        fun newInstance(): CameraFragment =
            CameraFragment()
    }
}