package com.example.mlgroceryshopping.camera

import android.app.DatePickerDialog
import android.content.Context
import android.text.Editable
import android.text.method.KeyListener
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import com.example.mlgroceryshopping.util.dateFormat
import org.json.JSONException
import org.json.JSONObject
import java.io.InputStream
import java.time.LocalDate
import java.util.*


class ListItemAdapter(var mCtx:Context , var resource:Int,var items:MutableList<String>)
    :ArrayAdapter<String>( mCtx , resource , items ){
    private lateinit var picker: DatePickerDialog

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val layoutInflater :LayoutInflater = LayoutInflater.from(mCtx)

        val view : View = layoutInflater.inflate(resource , null )
        var foodName : EditText = view.findViewById(com.example.mlgroceryshopping.R.id.foodName)
        var purchaseDate : EditText = view.findViewById(com.example.mlgroceryshopping.R.id.purchaseLabel)
        var expirationDate: EditText = view.findViewById(com.example.mlgroceryshopping.R.id.expLabel)
        var deleteItem: ImageButton = view.findViewById(com.example.mlgroceryshopping.R.id.deleteItem)
        var food : String = items[position]
        foodName.setText(food)
        purchaseDate.setText(LocalDate.now().toString())
        val expData: Int? = checkForData(food)
        if (expData == null) {
            expirationDate?.setText(LocalDate.now().plusDays(14).toString())
        } else {
            expirationDate?.setText(LocalDate.now().plusDays(expData.toLong()).toString())
        }
        expirationDate.setOnClickListener {
            val cldr = Calendar.getInstance()
            val day = cldr[Calendar.DAY_OF_MONTH]
            val month = cldr[Calendar.MONTH]
            val year = cldr[Calendar.YEAR]
            // date picker dialog
            // date picker dialog
            picker = DatePickerDialog(
                context,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth
                    ->
                    expirationDate.setText(
                        year.toString() + "-" + (monthOfYear + 1).toString()
                            .dateFormat() + "-" + dayOfMonth.toString().dateFormat()
                    )
                },
                year,
                month,
                day
            )
            picker.show()
        }
        purchaseDate.setOnClickListener {
            val cldr = Calendar.getInstance()
            val day = cldr[Calendar.DAY_OF_MONTH]
            val month = cldr[Calendar.MONTH]
            val year = cldr[Calendar.YEAR]
            // date picker dialog
            // date picker dialog
            picker = DatePickerDialog(
                context,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth
                    ->
                    purchaseDate.setText(
                        year.toString() + "-" + (monthOfYear + 1).toString()
                            .dateFormat() + "-" + dayOfMonth.toString().dateFormat()
                    )
                },
                year,
                month,
                day
            )
            picker.show()
        }
        deleteItem.setOnClickListener {
            items.removeAt(position)
            notifyDataSetChanged()
        }
        return view
    }

    private fun checkForData(name: String): Int? {
        var expDate: Int? = null
        var tmp: Int? = 0
        var foodsInfo: JSONObject? = loadJSON(context)
        for (foodKey in foodsInfo!!.keys()) {
            try {
                val t = foodsInfo.getJSONObject(foodKey)
                tmp = t.get(name.toLowerCase()) as Int?
            } catch (e: JSONException) {
                tmp = null
            }
            if (tmp != null) {
                break
            }
        }
        expDate = tmp
        return expDate
    }


    private fun loadJSON(context: Context): JSONObject? {
        var jsonData: String? = null;
        var istream: InputStream = context.assets.open("data.json")
        var size: Int = istream.available()
        var buff: ByteArray = ByteArray(size)
        istream.read(buff)
        istream.close()
        jsonData = String(buff)
        return JSONObject(jsonData)
    }

}