package com.example.mlgroceryshopping.food_modeldb

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.mlgroceryshopping.food_modeldb.Food
import kotlinx.coroutines.flow.Flow

@Dao
interface FoodDatabaseDao {
    @Query("SELECT * FROM food_table")
    fun getAll(): LiveData<List<Food>>

    @Query("SELECT * FROM food_table WHERE id IN (:foodIds)")
    fun loadAllByIds(foodIds: IntArray): List<Food>

    @Query("SELECT * FROM food_table WHERE id=(SELECT max(id) FROM food_table)")
    fun getLastEntry(): LiveData<Food>

    @Insert
    suspend fun insert(food: Food)
    //fun insertAll(vararg foods: Food)

    @Delete
    fun delete(food: Food)

    @Query("DELETE FROM food_table")
    suspend fun deleteAll()


}