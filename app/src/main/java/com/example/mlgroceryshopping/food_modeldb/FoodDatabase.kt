package com.example.mlgroceryshopping.food_modeldb

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.time.LocalDate

// Annotates class to be a Room Database with a table (entity) of the Word class
@Database(entities = arrayOf(Food::class), version = 5, exportSchema = false)
@TypeConverters(Converters::class)

public abstract class FoodDatabase : RoomDatabase() {

    abstract fun foodDao(): FoodDatabaseDao

    companion object {
        // Singleton prevents multiple instances of database opening at the
        // same time.
        @Volatile
        private var INSTANCE: FoodDatabase? = null

        fun getDatabase(context: Context): FoodDatabase {
            val tempInstance =
                INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    FoodDatabase::class.java,
                    "word_database"
                ).allowMainThreadQueries().fallbackToDestructiveMigration().build()
                INSTANCE = instance
                return instance
            }
        }
    }
//
//    private class FoodDatabaseCallback(
//        private val scope: CoroutineScope
//    ) : RoomDatabase.Callback() {
//
//        override fun onOpen(db: SupportSQLiteDatabase) {
//            super.onOpen(db)
//            INSTANCE?.let { database ->
//                scope.launch {
//                    populateDatabase(database.foodDao())
//                }
//            }
//        }
//
//        suspend fun populateDatabase(foodDao: FoodDatabaseDao) {
//            // Delete all content here.
//            foodDao.deleteAll()
//
//            // Add sample words.
//            //var food = Food("test")
//            foodDao.insert(
//                Food(
//                    foodName = "Test",
//                    purchaseDate = LocalDate.now(),
//                    expirationDate = LocalDate.now()
//                )
//            )
//
//        }
//    }
}