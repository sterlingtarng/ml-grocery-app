package com.example.mlgroceryshopping.food_modeldb

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

class FoodViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: FoodRepository
    // Using LiveData and caching what getAlphabetizedWords returns has several benefits:
    // - We can put an observer on the data (instead of polling for changes) and only update the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.
    val allFoods: LiveData<List<Food>>
    val lastEntry: LiveData<Food>

    init {
        val foodsDao = FoodDatabase.getDatabase(application).foodDao()
        repository =
            FoodRepository(foodsDao)
        allFoods = repository.allFoods
        lastEntry = repository.lastFoodEntry
    }

    /**
     * Launching a new coroutine to insert the data in a non-blocking way
     */
    fun insert(food: Food) = viewModelScope.launch(Dispatchers.IO) {
        repository.insert(food)
    }

    fun deleteFood(food: Food) = viewModelScope.launch {
        repository.deleteEntry(food)
    }

    fun deleteAll() = viewModelScope.launch {
        // 2
        repository.deleteAll()
    }
}