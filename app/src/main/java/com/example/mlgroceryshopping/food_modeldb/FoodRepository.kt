package com.example.mlgroceryshopping.food_modeldb

import androidx.lifecycle.LiveData
import com.example.mlgroceryshopping.food_modeldb.Food
import com.example.mlgroceryshopping.food_modeldb.FoodDatabaseDao
import kotlinx.coroutines.flow.Flow

// Declares the DAO as a private property in the constructor. Pass in the DAO
// instead of the whole database, because you only need access to the DAO
class FoodRepository(private val foodDao: FoodDatabaseDao) {

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    val allFoods: LiveData<List<Food>> = foodDao.getAll()

    val lastFoodEntry: LiveData<Food> = foodDao.getLastEntry()

    suspend fun insert(food: Food) {
        foodDao.insert(food)
    }

    suspend fun deleteEntry(food:Food) {
        foodDao.delete(food)
    }

    suspend fun deleteAll() {
        foodDao.deleteAll()
    }
}