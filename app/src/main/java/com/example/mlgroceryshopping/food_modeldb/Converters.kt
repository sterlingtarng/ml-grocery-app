package com.example.mlgroceryshopping.food_modeldb

import androidx.room.TypeConverter
import java.time.LocalDate

class Converters {
    @TypeConverter
    fun toDate(dateString: String?): LocalDate? {
        return if (dateString == null) {
            null
        } else {
            LocalDate.parse(dateString)
        }
    }

    @TypeConverter
    fun toDateString(date: LocalDate?): String? {
        return if (date == null) {
            null
        } else {
            date.toString()
        }
    }
//    @TypeConverter
//    fun fromTimestamp(value: Long?): LocalDate? {
//        return if (value == null) null else LocalDate.ofEpochDay(value)
//    }
//
//    @TypeConverter
//    fun dateToTimestamp(date: LocalDate?): Long? {
//        return date?.toEpochDay()
//    }

}