package com.example.mlgroceryshopping.food_modeldb

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import java.time.LocalDate
import java.time.LocalDateTime

@Entity(tableName = "food_table")
data class Food (
    @PrimaryKey(autoGenerate = true) var id: Int,
    @ColumnInfo(name = "food_name") val foodName: String?,
    @ColumnInfo(name = "purchase_date") val purchaseDate: LocalDate? = LocalDate.now(),
    @ColumnInfo(name = "expiration_date") val exp_Date: LocalDate? = LocalDate.now().plusDays(14)
) {
        constructor(foodName: String, purchaseDate: LocalDate?,  exp_Date: LocalDate?) : this(0, foodName, purchaseDate, exp_Date)
}