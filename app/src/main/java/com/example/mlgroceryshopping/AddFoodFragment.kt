package com.example.mlgroceryshopping



import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.room.Room
import com.example.mlgroceryshopping.food_modeldb.FoodDatabase


class AddFoodFragment : Fragment() {
    lateinit var db : FoodDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_add_food, container, false)

        db = context?.let {
            Room.databaseBuilder(
                it,
                FoodDatabase::class.java, "food-table"
            ).allowMainThreadQueries().build()
        }!!

        val editText = view.findViewById(R.id.foodNameEditText) as EditText
        editText.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                //Perform Code
                return@OnKeyListener true
            }
            false
        })

        val addFoodButton = view.findViewById<Button>(R.id.addFoodButton)
        addFoodButton.setOnClickListener{
            //addFood()
            val data = db.foodDao().getAll()
//            data.forEach{
//                Log.d("ID", it.uid.toString())
//                Log.d("foodname", it.foodName.toString())
//                Log.d("quantity", it.quantity.toString())
//                Log.d("purchasedate", it.purchaseDate.toString())
//                Log.d("expirationdate", it.expirationDate.toString())
//            }

            (activity as MainActivity?)?.openFragment(MainActivity().foodFragment)
        }

        return view
    }

    companion object {
        fun newInstance(): AddFoodFragment = AddFoodFragment()
    }

//    private fun addFood(){
//        Log.d("TAG", "Add Food")
//        db.foodDao().insertAll(Food(
//            foodName = foodNameEditText.text.toString(),
//            quantity = Integer.parseInt(quantityEditText.text.toString()),
//            purchaseDate = purchaseDateEditText.text.toString(),
//            expirationDate = expirationDateEditText.text.toString()
//        ))
//    }
}
