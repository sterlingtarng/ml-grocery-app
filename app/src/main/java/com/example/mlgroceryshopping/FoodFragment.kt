package com.example.mlgroceryshopping

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.example.mlgroceryshopping.food_modeldb.Food
import com.example.mlgroceryshopping.food_modeldb.FoodDatabase
import com.example.mlgroceryshopping.food_modeldb.FoodViewModel
import com.example.mlgroceryshopping.manual_entry_final.FoodListAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton
import java.time.LocalDate

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [FoodFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class FoodFragment : Fragment() {
    lateinit var db: FoodDatabase
    private lateinit var foodViewModel: FoodViewModel
    private val newWordActivityRequestCode = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_food, container, false)
        var recyclerView = view.findViewById(R.id.recyclerview) as RecyclerView
        //recycler view
        //val recyclerView = (activity as MainActivity).findViewById<RecyclerView>(R.id.recyclerview)
        val adapter =
            FoodListAdapter(view.context)

        recyclerView.adapter = adapter


        recyclerView.layoutManager = LinearLayoutManager(view.context)


        foodViewModel = ViewModelProvider(this).get(FoodViewModel::class.java)

        foodViewModel.allFoods.observe(viewLifecycleOwner, Observer { foods ->
            // Update the cached copy of the words in the adapter.
            foods?.let {
                adapter?.setFoods(it)
            }
        })
        view.findViewById<FloatingActionButton>(R.id.fab)?.setOnClickListener {
            val intent = Intent((activity as MainActivity?), NewFoodActivity::class.java)
            startActivityForResult(intent, newWordActivityRequestCode)
        }

        //get instance of database
        db = context?.let {
            Room.databaseBuilder(
                it,
                FoodDatabase::class.java, "food-table"
            ).allowMainThreadQueries().fallbackToDestructiveMigration().build()
        }!!


        //set delete database button
        val deleteButton = view.findViewById<Button>(R.id.deleteButton)
        deleteButton.setOnClickListener {
            Log.d("TAG", "delete BUTTON")
            foodViewModel.deleteAll()
        }

        return view
    }


    companion object {
        fun newInstance(): FoodFragment = FoodFragment()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == newWordActivityRequestCode && resultCode == Activity.RESULT_OK) {
            //data?.get
            data?.extras?.let {
                val foodN: String = it.get("foodName") as String
                val purDate: LocalDate = it.get("purDate") as LocalDate
                val expDate: LocalDate = it.get("expDate") as LocalDate
                val food = Food(foodN, purDate, expDate)
                foodViewModel.insert(food)
            }
//            val data = db.foodDao().getAll().value
//            data?.forEach{
//                Log.d("ID", it.id.toString())
//                Log.d("foodname", it.foodName.toString())
//                Log.d("quantity", it.quantity.toString())
//                Log.d("purchasedate", it.purchaseDate.toString())
//                Log.d("expirationdate", it.expirationDate.toString())
//            }
        } else {
            Toast.makeText(
                (activity as MainActivity?)?.applicationContext,
                R.string.empty_not_saved,
                Toast.LENGTH_LONG
            ).show()
        }
    }


}
