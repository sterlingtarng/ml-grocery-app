package com.example.mlgroceryshopping

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.example.mlgroceryshopping.food_modeldb.Food
import com.example.mlgroceryshopping.food_modeldb.FoodDatabase
import com.example.mlgroceryshopping.food_modeldb.FoodViewModel
import com.example.mlgroceryshopping.manual_entry_final.DataPopupFragment
import com.example.mlgroceryshopping.manual_entry_final.FoodListAdapter
import kotlinx.android.synthetic.main.fragment_home.view.*
import java.time.LocalDate

class HomeFragment : Fragment() {
    lateinit var db: FoodDatabase
    lateinit var foodViewModel: FoodViewModel
    private val newWordActivityRequestCode = 1
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_home, container, false)

        var recyclerView = view.findViewById(com.example.mlgroceryshopping.R.id.recyclerview) as RecyclerView
        //recycler view
        val adapter =
            FoodListAdapter(view.context)

        var deleteAll = view.findViewById<Button>(com.example.mlgroceryshopping.R.id.deleteAll)


        recyclerView.adapter = adapter

        recyclerView.layoutManager = LinearLayoutManager(view.context)

        foodViewModel = ViewModelProvider(this).get(FoodViewModel::class.java)

        foodViewModel.allFoods.observe(viewLifecycleOwner, Observer { foods ->
            // Update the cached copy of the words in the adapter.
            foods?.let {
                adapter.setFoods(it)
            }
        })

        //get instance of database
        db = context?.let {
            Room.databaseBuilder(
                it,
                FoodDatabase::class.java, "food-table"
            ).allowMainThreadQueries().fallbackToDestructiveMigration().build()
        }!!

        deleteAll.setOnClickListener {
            foodViewModel.deleteAll()
        }
        return view
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == newWordActivityRequestCode && resultCode == Activity.RESULT_OK) {
            //data?.get
            data?.extras?.let {
                val foodN: String = it.get("foodName") as String
                val purDate: LocalDate = it.get("purDate") as LocalDate
                val expDate: LocalDate = it.get("expDate") as LocalDate
                val food = Food(foodN, purDate, expDate)
                foodViewModel.insert(food)
            }
//            data?.getStringExtra(NewManualFoodActivity.EXTRA_REPLY)?.let {
//                val food = Food(it)
//                foodViewModel.insert(food)
//
//            }
//            val data = db.foodDao().getAll().value
//            data?.forEach{
//                Log.d("ID", it.id.toString())
//                Log.d("foodname", it.foodName.toString())
//                Log.d("quantity", it.quantity.toString())
//                Log.d("purchasedate", it.purchaseDate.toString())
//                Log.d("expirationdate", it.expirationDate.toString())
//            }
        } else {
            Toast.makeText(
                (activity as MainActivity?)?.applicationContext,
                com.example.mlgroceryshopping.R.string.empty_not_saved,
                Toast.LENGTH_LONG
            ).show()
        }
    }
        //return view


    companion object {
        fun newInstance(): HomeFragment = HomeFragment()
    }



}