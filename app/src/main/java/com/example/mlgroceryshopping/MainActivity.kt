package com.example.mlgroceryshopping

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import com.example.mlgroceryshopping.camera.CameraFragment
import com.example.mlgroceryshopping.food_modeldb.Food
import com.example.mlgroceryshopping.food_modeldb.FoodDatabase
import com.example.mlgroceryshopping.food_modeldb.FoodViewModel
import com.example.mlgroceryshopping.manual_entry_final.DashboardFragment
import com.example.mlgroceryshopping.notifications.BroadcastReceiverFood
import com.example.mlgroceryshopping.notifications.NotificationsFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import java.io.InputStream
import java.time.LocalDate


class MainActivity : AppCompatActivity() {
    var listNots: MutableList<MutableMap<String, String>> = mutableListOf()
    val foodFragment = FoodFragment.newInstance()
    val homeFragment = HomeFragment.newInstance()
    private val dashboardFragment = DashboardFragment.newInstance()
    private val notificationsFragment = NotificationsFragment.newInstance(listNots)
    lateinit var foodViewModel: FoodViewModel
    private val NOTIF_EXPIRED_KEY = "NOTIFICATION_EXPIRED_GROUP"
    private val NOTIF_WARNING_KEY = "NOTIFICATION_WARNING_GROUP"

    lateinit var db: FoodDatabase
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                openFragment(homeFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_camera -> {
                val camFragment = CameraFragment.newInstance()
//                camFragment.message.setText(R.string.title_camera)
                openFragment(camFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_manual_entry -> {
                openFragment(dashboardFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_notifications -> {
                openFragment(notificationsFragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val bottomNavigation: BottomNavigationView = findViewById(R.id.navigation)
        openFragment(homeFragment)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        db = Room.databaseBuilder(
            applicationContext,
            FoodDatabase::class.java, "food-table"
        ).allowMainThreadQueries().build()

        foodViewModel = ViewModelProvider(this).get(FoodViewModel::class.java)

        foodViewModel.allFoods.observe(this, Observer { foods ->
            // Update the cached copy of the words in the adapter.
            foods?.let {
                displayExpirationNotifications(it)
            }
        })

        createNotificationChannel();
    }

    private fun displayExpirationNotifications(foodList: List<Food>) {
        val notificationManager = NotificationManagerCompat.from(this)
        val stateIntent = Intent(this, BroadcastReceiverFood::class.java)
        stateIntent.putExtra("id", 100)
        val today = LocalDate.now()
        for (food in foodList) {
            if (today.compareTo(food.exp_Date) == 0) {
                val foodname = food.foodName
                val foodTitle = foodname + " is about to expire"
                val foodText = "See if you can use it up by today"
                val builder: NotificationCompat.Builder = NotificationCompat.Builder(this, "mlg_notification")
                    .setSmallIcon(com.example.mlgroceryshopping.R.drawable.ic_home_black_24dp)
                    .setContentTitle(foodTitle)
                    .setContentText(foodText)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setGroup(NOTIF_WARNING_KEY)
                val noti: MutableMap<String, String> = mutableMapOf("Title" to foodTitle, "Message" to foodText)
                notificationManager.notify(100, builder.build())
                listNots.add(noti)
            } else if (today.compareTo(food.exp_Date) > 0 ) {
                val foodname = food.foodName
                val foodTitle = foodname + " has expired"
                val foodText = "Plrease throw it away"
                val builder: NotificationCompat.Builder = NotificationCompat.Builder(this, "mlg_notification")
                    .setSmallIcon(com.example.mlgroceryshopping.R.drawable.ic_home_black_24dp)
                    .setContentTitle(foodTitle)
                    .setContentText(foodText)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setGroup(NOTIF_EXPIRED_KEY)
                val noti: MutableMap<String, String> = mutableMapOf("Title" to foodTitle, "Message" to foodText)
                notificationManager.notify(100, builder.build())
                listNots.add(noti)
            }
        }
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name: CharSequence = "studentChannel"
            val description = "Channel for student notifications"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel("mlg_notification", name, importance)
            channel.description = description
            val notificationManager = getSystemService(
                NotificationManager::class.java
            )
            notificationManager?.createNotificationChannel(channel)
        }
    }
}
