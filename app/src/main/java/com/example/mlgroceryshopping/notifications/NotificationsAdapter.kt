package com.example.mlgroceryshopping.notifications

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.mlgroceryshopping.R
import java.util.*

class NotificationsAdapter(var context: Context):RecyclerView.Adapter<NotificationsAdapter.NotificationsViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var notis: MutableList<MutableMap<String, String>> = mutableListOf()// Cached copy of words

    inner class NotificationsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val notiTitle: TextView = itemView.findViewById(R.id.Title)
        val notiMessage: TextView = itemView.findViewById(R.id.Message)
        val deleteButton: ImageButton = itemView.findViewById(R.id.deleteItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationsViewHolder {
        val itemView = inflater.inflate(R.layout.recyclerview_item_notif, parent, false)
        return NotificationsViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: NotificationsViewHolder, position: Int) {
        val current = notis[position]
        holder.notiTitle.text = current["Title"]
        holder.notiMessage.text = current["Message"]
        holder.deleteButton.setOnClickListener {
            notis.removeAt(position)
            notifyDataSetChanged()
        }
    }

    internal fun addNoti(noti: MutableMap<String, String>) {
        this.notis.add(noti)
        notifyDataSetChanged()
    }

    internal fun setNotis(nots: MutableList<MutableMap<String, String>>) {
        this.notis = nots
        notifyDataSetChanged()
    }

    override fun getItemCount() = notis.size
}