package com.example.mlgroceryshopping.notifications

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.core.app.NotificationManagerCompat


class BroadcastReceiverFood : BroadcastReceiver() {
    override fun onReceive(p0: Context, p1: Intent?) {
        val notificationId: Int = p1!!.getIntExtra("id", 0)
        val notificationManager = NotificationManagerCompat.from(p0)
        notificationManager.cancel(notificationId)

        val sharedPref: SharedPreferences =
            p0.getSharedPreferences("mlg_notification_pref", Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putString("mlg_notification", "Active")
        editor.apply()    }
}