package com.example.mlgroceryshopping.notifications

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context.ALARM_SERVICE
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.time.LocalDate
import java.util.*


class  NotificationsFragment(list: MutableList<MutableMap<String, String>>) : Fragment() {
    var notificationList = list
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreate(savedInstanceState)
        val view = inflater.inflate(com.example.mlgroceryshopping.R.layout.fragment_notifications, container, false)
        var recyclerView = view.findViewById(com.example.mlgroceryshopping.R.id.recyclerview) as RecyclerView
        val adapter =
            NotificationsAdapter(view.context)

        recyclerView.adapter = adapter

        recyclerView.layoutManager = LinearLayoutManager(view.context)

        adapter.setNotis(notificationList)

        return view
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name: CharSequence = "studentChannel"
            val description = "Channel for student notifications"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel("lemubitA", name, importance)
            channel.description = description
            val notificationManager = getSystemService(
                this.requireContext(), NotificationManager::class.java
            )
            notificationManager?.createNotificationChannel(channel)
        }
    }

    companion object {
        fun newInstance(list: MutableList<MutableMap<String, String>>): NotificationsFragment =
            NotificationsFragment(list)
    }

}

