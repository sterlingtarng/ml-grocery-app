package com.example.mlgroceryshopping.manual_entry_final

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.example.mlgroceryshopping.R
import com.example.mlgroceryshopping.food_modeldb.Food
import com.example.mlgroceryshopping.food_modeldb.FoodDatabase

class FoodListAdapter(var context: Context):RecyclerView.Adapter<FoodListAdapter.FoodViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var foods = emptyList<Food>() // Cached copy of words
    lateinit var db: FoodDatabase

    inner class FoodViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val foodItemView: TextView = itemView.findViewById(R.id.foodName)
        val expDateView: TextView = itemView.findViewById(R.id.expDate)
//        val editButton: ImageButton = itemView.findViewById(R.id.editItem)
        val deleteButton: ImageButton = itemView.findViewById(R.id.deleteItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodViewHolder {
        val itemView = inflater.inflate(R.layout.recyclerview_item, parent, false)
//        db = context?.let {
//            Room.databaseBuilder(
//                it,
//                FoodDatabase::class.java, "food-table"
//            ).allowMainThreadQueries().fallbackToDestructiveMigration().build()
//        }!!
        return FoodViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: FoodViewHolder, position: Int) {
        val current = foods[position]
        holder.foodItemView.text = current.foodName
        holder.expDateView.text = current.exp_Date.toString()
        holder.deleteButton.setOnClickListener {
//            Log.d("Here", current.foodName)
//            val f: Food = current
//            db.foodDao().delete(f)
//            notifyDataSetChanged()
            deleteEntry(current, position)
//            foods.drop(position)
//            notifyItemRemoved(position)
        }
    }

    internal fun setFoods(foods: List<Food>) {
        this.foods = foods
        notifyDataSetChanged()
    }

    internal fun deleteEntry(current: Food, position: Int) {
        val dbs: FoodDatabase = FoodDatabase.getDatabase(context)
        dbs.foodDao().delete(current)
        var newFoods = this.foods.toMutableList().removeAt(position)
        this.foods = listOf(newFoods)
        notifyDataSetChanged()
    }


    override fun getItemCount() = foods.size
}