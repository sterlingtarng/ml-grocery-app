package com.example.mlgroceryshopping.manual_entry_final

import android.app.Activity
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.text.TextUtils
import android.util.Log
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.example.mlgroceryshopping.util.dateFormat
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.ZoneId
import java.util.*


class NewManualFoodActivity : AppCompatActivity() {
    private lateinit var editWordView: EditText
    private lateinit var editPurchaseDateView: EditText
    private lateinit var editExpirationDateView: EditText
    private lateinit var picker: DatePickerDialog

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.example.mlgroceryshopping.R.layout.activity_new_manual_food_entry)
        editWordView = findViewById(com.example.mlgroceryshopping.R.id.editProductName)
        editPurchaseDateView = findViewById(com.example.mlgroceryshopping.R.id.editPurchaseDate)
        editExpirationDateView = findViewById(com.example.mlgroceryshopping.R.id.editExpirationDate)

        val button = findViewById<Button>(com.example.mlgroceryshopping.R.id.button)

        editPurchaseDateView.setOnClickListener {
            val cldr = Calendar.getInstance()
            val day = cldr[Calendar.DAY_OF_MONTH]
            val month = cldr[Calendar.MONTH]
            val year = cldr[Calendar.YEAR]
            // date picker dialog
            // date picker dialog
            picker = DatePickerDialog(
                this,
                OnDateSetListener { view, year, monthOfYear, dayOfMonth
                    -> editPurchaseDateView.setText(year.toString() + "-" + (monthOfYear + 1).toString().dateFormat() + "-" + dayOfMonth.toString().dateFormat()) },
                year,
                month,
                day
            )
            picker.show()
        }

        editExpirationDateView.setOnClickListener {
            val cldr = Calendar.getInstance()
            val day = cldr[Calendar.DAY_OF_MONTH]
            val month = cldr[Calendar.MONTH]
            val year = cldr[Calendar.YEAR]
            // date picker dialog
            // date picker dialog
            picker = DatePickerDialog(
                this,
                OnDateSetListener { view, year, monthOfYear, dayOfMonth
                    -> editExpirationDateView.setText(year.toString() + "-" + (monthOfYear + 1).toString().dateFormat() + "-" + dayOfMonth.toString().dateFormat()) },
                year,
                month,
                day
            )
            picker.show()
        }

        button.setOnClickListener {
            val replyIntent = Intent()
            if (TextUtils.isEmpty(editWordView.text)) {
                setResult(Activity.RESULT_CANCELED, replyIntent)
            } else {
                var b: Bundle = Bundle()
                val df = SimpleDateFormat("yyyy-MM-dd")
                val myDate1: Date = df.parse(editPurchaseDateView.text.toString())
                val myDate2: Date = df.parse(editExpirationDateView.text.toString())
                val locald = myDate1.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
//                val k = myDate1.toInstant().atZone().toLocalDate()
                Log.d("DATE", myDate1.toString())
                var day1 = locald.dayOfMonth
                var year1 = locald.year
                var mon1 = locald.month
                var d1 = LocalDate.of(year1, mon1, day1)
                val locald2 = myDate2.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                var day2 = locald2.dayOfMonth
                var year2 = locald2.year
                var mon2 = locald2.month
                var d2 = LocalDate.of(year2, mon2, day2)
                b.putSerializable("foodName", editWordView.text.toString())
                b.putSerializable("purDate", d1)
                b.putSerializable("expDate", d2)
                replyIntent.putExtras(b)
                setResult(Activity.RESULT_OK, replyIntent)
            }
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus?.windowToken, 0)
            finish()
        }
    }

    companion object {
        const val EXTRA_REPLY = "com.example.android.wordlistsql.REPLY"
    }
}