package com.example.mlgroceryshopping.manual_entry_final

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import com.example.mlgroceryshopping.MainActivity
import com.example.mlgroceryshopping.R
import com.example.mlgroceryshopping.food_modeldb.Food
import com.example.mlgroceryshopping.food_modeldb.FoodDatabase
import com.example.mlgroceryshopping.food_modeldb.FoodViewModel
import java.time.LocalDate

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class DashboardFragment : Fragment() {
    lateinit var db: FoodDatabase
    lateinit var foodViewModel: FoodViewModel
    private val newWordActivityRequestCode = 1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_manual_entry, container, false)
        //recycler view

        view.findViewById<Button>(R.id.button)?.setOnClickListener {
            val intent = Intent((activity as MainActivity?), NewManualFoodActivity::class.java)
            startActivityForResult(intent, newWordActivityRequestCode)
        }
        foodViewModel = ViewModelProvider(this).get(FoodViewModel::class.java)


        //get instance of database
        db = context?.let {
            Room.databaseBuilder(
                it,
                FoodDatabase::class.java, "food-table"
            ).allowMainThreadQueries().fallbackToDestructiveMigration().build()
        }!!

        val viewData = view.findViewById<Button>(R.id.button2)
        viewData.setOnClickListener {
            val fm = childFragmentManager
            val fragmentListPopup: DataPopupFragment = DataPopupFragment.newInstance()
            fragmentListPopup.show(fm, "DialogFragment")
        }


        val lastEntryFoodName = view.findViewById<TextView>(com.example.mlgroceryshopping.R.id.lastEntry)
        val lastEntryExpirationDate = view.findViewById<TextView>(com.example.mlgroceryshopping.R.id.expirationDate)
        val lastEntryPurchaseDate = view.findViewById<TextView>(com.example.mlgroceryshopping.R.id.purchaseDate)


////        lastEntry.setText(foodViewModel.lastEntry.toString())
//        val entryObserver = Observer<Food> { lastEntryIn ->
//            if (lastEntryIn.foodName != null) {
//                lastEntryFoodName.setText(lastEntryIn.foodName)
//            }
//        }
        foodViewModel.lastEntry.observe(viewLifecycleOwner, Observer {lastEntryIn ->
            lastEntryIn?.let {
                lastEntryFoodName.setText(lastEntryIn.foodName)
                lastEntryExpirationDate.setText(lastEntryIn.exp_Date.toString())
                lastEntryPurchaseDate.setText(lastEntryIn.purchaseDate.toString())
            }
        })

        return view
    }

    companion object {
        fun newInstance(): DashboardFragment =
            DashboardFragment()
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == newWordActivityRequestCode && resultCode == Activity.RESULT_OK) {
            //data?.get
            data?.extras?.let {
                val foodN: String = it.get("foodName") as String
                val purDate: LocalDate = it.get("purDate") as LocalDate
                val expDate: LocalDate = it.get("expDate") as LocalDate
                val food = Food(foodN, purDate, expDate)
                foodViewModel.insert(food)
            }
//            val data = db.foodDao().getAll().value
//            data?.forEach{
//                Log.d("ID", it.id.toString())
//                Log.d("foodname", it.foodName.toString())
//                Log.d("quantity", it.quantity.toString())
//                Log.d("purchasedate", it.purchaseDate.toString())
//                Log.d("expirationdate", it.expirationDate.toString())
//            }
        } else {
            Toast.makeText(
                (activity as MainActivity?)?.applicationContext,
                R.string.empty_not_saved,
                Toast.LENGTH_LONG
            ).show()
        }
    }
}
