package com.example.mlgroceryshopping.manual_entry_final

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.example.mlgroceryshopping.MainActivity
import com.example.mlgroceryshopping.food_modeldb.Food
import com.example.mlgroceryshopping.food_modeldb.FoodDatabase
import com.example.mlgroceryshopping.food_modeldb.FoodViewModel
import java.time.LocalDate

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class DataPopupFragment : DialogFragment() {
    lateinit var db: FoodDatabase
    lateinit var foodViewModel: FoodViewModel
    private val newWordActivityRequestCode = 1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,

        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(com.example.mlgroceryshopping.R.layout.fragment_list_popup, container, false)

        var recyclerView = view.findViewById(com.example.mlgroceryshopping.R.id.recyclerview) as RecyclerView
        //recycler view
        val adapter =
            FoodListAdapter(view.context)

        var deleteAll = view.findViewById<Button>(com.example.mlgroceryshopping.R.id.deleteAll)


        recyclerView.adapter = adapter

        recyclerView.layoutManager = LinearLayoutManager(view.context)

        foodViewModel = ViewModelProvider(this).get(FoodViewModel::class.java)

        foodViewModel.allFoods.observe(viewLifecycleOwner, Observer { foods ->
            // Update the cached copy of the words in the adapter.
            foods?.let {
                adapter.setFoods(it)
            }
        })

        //get instance of database
        db = context?.let {
            Room.databaseBuilder(
                it,
                FoodDatabase::class.java, "food-table"
            ).allowMainThreadQueries().fallbackToDestructiveMigration().build()
        }!!

        deleteAll.setOnClickListener {
            foodViewModel.deleteAll()
        }
        return view
    }


    companion object {
        fun newInstance(): DataPopupFragment = DataPopupFragment()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == newWordActivityRequestCode && resultCode == Activity.RESULT_OK) {
            //data?.get
            data?.extras?.let {
                val foodN: String = it.get("foodName") as String
                val purDate: LocalDate = it.get("purDate") as LocalDate
                val expDate: LocalDate = it.get("expDate") as LocalDate
                val food = Food(foodN, purDate, expDate)
                foodViewModel.insert(food)
            }
//            data?.getStringExtra(NewManualFoodActivity.EXTRA_REPLY)?.let {
//                val food = Food(it)
//                foodViewModel.insert(food)
//
//            }
//            val data = db.foodDao().getAll().value
//            data?.forEach{
//                Log.d("ID", it.id.toString())
//                Log.d("foodname", it.foodName.toString())
//                Log.d("quantity", it.quantity.toString())
//                Log.d("purchasedate", it.purchaseDate.toString())
//                Log.d("expirationdate", it.expirationDate.toString())
//            }
        } else {
            Toast.makeText(
                (activity as MainActivity?)?.applicationContext,
                com.example.mlgroceryshopping.R.string.empty_not_saved,
                Toast.LENGTH_LONG
            ).show()
        }
    }


}
